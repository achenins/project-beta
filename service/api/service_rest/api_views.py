from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AutomobileVO,
    TechnicianEncoder,
    ServiceAppointmentEncoder,
    ServiceAppointmentListEncoder,
)

from .models import (
    AutomobileVO,
    Technician,
    ServiceAppointment,
)


# Technicians
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Error creating a Technician"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        if request.method == "GET":
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        else:
            content = json.loads(request.body)
            props = ["name", "employee_number"]

            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
    except Technician.DoesNotExist:
        response = JsonResponse({"message": "Technician can't be found with this ID"})
        response.status_code = 404
        return response


# Service Appointments
@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            technician_id = content["technician"]
            technician = Technician.objects.get(employee_number=technician_id)
            content["technician"] = technician

            auto_vin = content["automobile"]
            automobile = AutomobileVO.objects.filter(vin=auto_vin).first()
            content["automobile"] = automobile

            if automobile.ispurchased is True:
                content["vip_status"] = True

            appointments = ServiceAppointment.objects.create(**content)
            return JsonResponse(
                appointments,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Failed to make a Service Appointment. Try again."}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, vin, pk):
    try:
        appointment = ServiceAppointment.objects.get(vin=vin, id=pk)

        if request.method == "GET":
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            appointment.delete()
            appointments = ServiceAppointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=ServiceAppointmentListEncoder,
            )
        else:  # PUT
            appointment = ServiceAppointment.objects.get(vin=vin, id=pk)
            appointment.complete()
            return JsonResponse({"message": "Status changed to 'Completed'"})

    except ServiceAppointment.DoesNotExist:
        return JsonResponse(
            {"message": "The Appointment ID given isn't valid. Try again."}
        )
