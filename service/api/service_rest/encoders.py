from common.json import ModelEncoder

from .models import (
    AutomobileVO,
    Technician,
    ServiceAppointment,
)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "ispurchased",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]


class ServiceAppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "owner_name",
        "datetime_of_appointment",
        "technician",
        "reason_for_appointment",
        "vip_status",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "owner_name",
        "datetime_of_appointment",
        "reason_for_appointment",
        "technician",
        "automobile",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "automobile": AutomobileVOEncoder(),
    }
