#   CarCar

 :red_car: :blue_car: **CarCar is a "premium" dealership inventory and customer manager application** :blue_car: :red_car:

> Easily add new vehicles to start managing inventory!<br>
> Simply manage employee and customer data!<br>
> Access to sales records and customer appointments!<br>
> Automatically update inventory when customers make a purchase!<br>
> Set up appointments for your VIP customers!<br>
___

CarCar Dev Team:
- Allen Chen -- Sales
- Jerry Joiner -- Service

## How to Run this Application:

**Installation**<br>
Please make sure you have the following installed:
- [Python 3](https://www.python.org/downloads/)
- [Docker](https://www.docker.com/get-started/)
- [Insomnia](https://insomnia.rest/pricing)

**Setup**<br>
Fork CarCar into your Git Projects
Clone CarCar to intended directory
```sh
cd <your directory>
git clone https://gitlab.com/achenins/project-beta.git
```
Go into the repo:
```sh
cd project-beta
```
If you wish to set up a virtual environment:
```sh
python -m venv .venv
source venv/bin/activate
```
To deactivate virtual environment:
```sh
deactivate
```
Make sure pip is up to date:
```sh
python -m pip install --upgrade pip
```
Install all dependencies from requirements.txt:
```sh
python install -r requirements.txt
```
**Docker**<br>
By default, CarCar containers run on port 8000. To use a different port, change it in the Dockerfile.

Local DB will utilize Postgres DB.
Set up docker volume for data persistence:
```sh
docker volume create beta-data
```
CarCar comes with docker-compose.yaml
Build the docker image:
```sh
docker-compose build
```
To run the containers:
```sh
docker-compose up
```
**Insomnia (optional)**<br>
For simple and quick API setup use the Insomnia client.

Create a new project:<br>

![create new project in insomnia](ghi/app/public/readmeimg/insomnia1.png)

***Refer to API Documentation for defined URLs***<br>
Create new HTTP Requests for API:<br>

![create new HTTP requests](ghi/app/public/readmeimg/insomnia2.png)


## Application Diagram


![Project Diagram](ghi/app/public/readmeimg/CarCarDiagram.png)


## Services


**GHI: localhost:3000 (React Front-End)**
- src: contains Javascript files for front-end
	- Inventory dir: contains all the front-end files for Inventory service
	- Sales dir: contains all the front-end files for Sales microservice
	- Service dir: contains all the front-end files for Service mircroservice
	- Nav.js: contains the Navigation Links for all services
		| Link | Web Page |
		| ----------- | ----------- |
		|   / |  Home |
		|   /manufacturer | View all Manufacturers |
		|   /manufacturer/new |  Add a new Manufacturer |
		|   /vehicle |  View all Vehicle Models |
		|   /vehicle/new  |  Add a new Vehicle Model |
		|   /automobiles |  View all Automobiles |
		|   /automobiles/new |  Add a new Automobiles |
		|   /salesrecords |  View all Sales Records |
		|   /salesrecords/select |  View Sales Record by Sales Person |
		|   /salesrecords/new |  Create a new Sales Record |
		|   /salespersons/new |  Add a new Sales Person |
		|   /salescustomers/new |  Add a new Sales (Potential) Customer |

**Inventory: locahost:8100 (Django Back-End)**
- inventory_rest: contains all Python files for inventory back-end
	- encoders.py: contains all inventory models encoders
	- models.py: all inventory models
	- urls.py: paths for inventory HTTP requests
		| URL Paths | View Function Names |
		| ----------- | ----------- |
		|   api/manufacturers/ |  api_manufacturers |
		|   api/manufacturers/<int:pk>/ | api_manufacturer |
		|   api/models/ |  api_vehicle_models |
		|   api/models/<int:pk>/ |  api_vehicle_model |
		|   api/automobiles/  |  api_automobiles |
		|   api/automobiles/<str:vin>/ |  api_automobile |
	- views.py: functions that convert HTTP requests to a response
		| View Function Names | Function |
		| ----------- | ----------- |
		|   api_manufacturers |  GET all Manufacturer data and POST (create) a new Manufacturer |
		|   api_manufacturer | GET a specific Manufacturer data, DELETE a specific Manufacturer, and PUT (update) a specific Manufacturer data |
		|   api_vehicle_models |  GET all Vehicle Model data and POST (create) a new Vehicle Model |
		|   api_vehicle_model |  GET a specific Vehicle Model data, DELETE a specific Vehicle Model, and PUT(update) a specific Vehicle Model data |
		|   api_automobiles  |  GET all Automobile data and POST (create) a new Automobile |
		|   api_automobile |  GET a specific Automobile data, DELETE a specific Automobile, and PUT(update) a specific Automobile data |
**Sales: localhost:8090 (Django Back-End)**
- sales_rest: contains all Python files for sales back-end
	- encoders.py: contains all sales models encoders
	- models.py: all sales models
	- urls.py: paths for inventory HTTP requests
		| URL Paths | View Function Names |
		| ----------- | ----------- |
		|   api/salespersons/ |  api_sales_persons |
		|   api/salespersons/<int:pk>/ | api_sales_person |
		|   api/salescustomers/ |  api_sales_customers |
		|   api/salescustomers/<int:pk>/ |  api_sales_customer |
		|   api/salesrecords/  |  api_sales_records |
		|   api/salesrecords/<int:pk>/ |  api_sales_record |
	- views.py: functions that convert HTTP requests to a response
		| View Function Names | Function |
		| ----------- | ----------- |
		|   api_sales_persons |  GET all Sales Person data and POST (create) a new Sales Person |
		|   api_sales_person | GET a specific Sales Person data, DELETE a specific Sales Person, and PUT (update) a specific Sales Person data |
		|   api_sales_customers |  GET all Sales Customer data and POST (create) a new Sales Customer |
		|   api_sales_customer |  GET a specific Sales Customer data, DELETE a specific Sales Customer, and PUT(update) a specific Sales Customer data |
		|   api_sales_records  |  GET all Sales Record data and POST (create) a new Sales Record |
		|   api_sales_record |  GET a specific Sales Record data and DELETE a specific Sales Record |
- Poll
	- Poller: Polls Automobile data from Inventory to fill data for AutomobileVO (value object)
**Service: localhost:8090 (Django Back-End)**
- service_rest: contains all Python files for service back-end
	- encoders.py: contains all service models encoders
	- models.py: all service models
	- urls.py: paths for inventory HTTP requests
		| URL Paths | View Function Names |
		| ----------- | ----------- |
		|   api/technicians/ |  api_list_technicians |
		|   api/technicians/<int:pk>/ | api_show_technician |
		|   api/appointments/|  api_list_appointments |
		|   api/appointments/<str:vin>/<int:pk>/  |  api_show_appointment |
	- views.py: functions that convert HTTP requests to a response
		| View Function Names | Function |
		| ----------- | ----------- |
		|   api_list_technicians |  GET all Technician data and POST (create) a new Technician Person |
		|   api_show_technician | GET a specific Technician's data, DELETE a specific Technician, and PUT (update) a specific Technician |
		|   api_list_appointments |  GET all Appointments data and POST (create) a new Service Appointment |
		|   api_show_appointment |  GET specific Service Appointment data, DELETE a specific Service Appointment, and PUT(update) a specific Service Appointment's data |
- Poll
	- Poller: Polls Automobile data from Inventory to fill data for AutomobileVO (value object)


## API Documentation

**Default Ports**<br>

Inventory: port 8100

Sales: port 8090

Service: port 8080

GHI(React): port 3000

*Docker Containers: port 8000*

**Routes for API**

| HTTP Requests | URLs |
| ----------- | ----------- |
|   GET List of Manufacturer |  http://localhost:8100/api/manufacturers/ |
|   GET Specific Manufacturer | http://localhost:8100/api/manufacturers/:id/ |
|   POST Manufacturer |  http://localhost:8100/api/manufacturers/ |
|   PUT Manufacturer |  http://localhost:8100/api/manufacturers/:id/ |
|   DELETE Manufacturer  |  http://localhost:8100/api/manufacturers/:id/ |
|   GET List of Vehicle Model |  http://localhost:8100/api/models/ |
|   GET Specific Vehicle Model | http://localhost:8100/api/models/:id/ |
|   POST Vehicle Model |  http://localhost:8100/api/models/ |
|   PUT Vehicle Model |  http://localhost:8100/api/models/:id/ |
|   DELETE Vehicle Model  |  http://localhost:8100/api/models/:id/ |
|   GET List of Automobile |  http://localhost:8100/api/automobiles/ |
|   GET Specific Automobile | http://localhost:8100/api/automobiles/:vin/ |
|   POST Automobile |  http://localhost:8100/api/automobiles/ |
|   PUT Automobile |  http://localhost:8100/api/automobiles/:vin/ |
|   DELETE Automobile  |  http://localhost:8100/api/automobiles/:vin/ |
|   GET List of Sales Person |  http://localhost:8090/api/salespersons/ |
|   GET Specific Sales Person | http://localhost:8090/api/salespersons/:id/ |
|   POST Sales Person |  http://localhost:8090/api/salespersons/ |
|   PUT Sales Person |  http://localhost:8090/api/salespersons/:id/ |
|   DELETE Sales Person  |  http://localhost:8090/api/salespersons/:id/ |
|   GET List of Sales Customer |  http://localhost:8090/api/salescustomers/ |
|   GET Specific Sales Customer | http://localhost:8090/api/salescustomers/:id/ |
|   POST Sales Customer |  http://localhost:8090/api/salescustomers/ |
|   PUT Sales Customer |  http://localhost:8090/api/salescustomers/:id/ |
|   DELETE Sales Customer  |  http://localhost:8090/api/salescustomers/:id/ |


|   GET List of Technicians |  http://localhost:8080/api/technicians/ |
|   GET Specific Technician | http://localhost:8080/api/technicians/:id/ |
|   POST Technician |  http://localhost:8080/api/technicians/ |
|   PUT Technician Person |  http://localhost:8080/api/technicians/:id/ |
|   DELETE Technician |  http://localhost:8080/api/technicians/:id/ |
|   GET List of Appointments |  http://localhost:8080/api/appointments/ |
|   GET Specific Appointment | http://localhost:8080/api/appointments/:vin/:id/ |
|   POST Appointment |  http://localhost:8080/api/appointments/ |
|   PUT Appointment |  http://localhost:8080/api/appointments/:vin/:id/ |
|   DELETE Appointment  |  http://localhost:8080/api/appointments/:vin/:id/ |

**Sample Request Body**

![sample request body](ghi/app/public/readmeimg/Sample Request.png)

**Sample Successful Response**

![sample success response](ghi/app/public/readmeimg/Sample Success Response.png)


## Value Objects


AutomobileVO
