import { useEffect, useState } from "react";
import React from 'react';

function SelectSalesRecords() {

    const [sales_records, setSalesRecord] = useState([]);
    const [sales_persons, setSalesPerson] = useState([]);
    const [selected_person, setSelectedPerson] = useState('')

    useEffect(() => {
        const salesRecordsURL = 'http://localhost:8090/api/salesrecords/';
        const salesPersonsURL = 'http://localhost:8090/api/salespersons/';
        const URLs = [salesRecordsURL, salesPersonsURL]
        Promise.all(URLs.map(URL => fetch(URL)
            .then(response => response.json())
        ))
            .then((data) => {
                setSalesRecord(data[0].sales_records);
                setSalesPerson(data[1].sales_persons);
            })
            .catch(e => console.error('ERROR: ', e))

    }, [])

    const handlePersonChange = (event) => {
        const value = event.target.value
        setSelectedPerson(value)
    }

    let tableClass = 'table table-striped d-none'
    if (selected_person !== '') {
        tableClass = 'table table-striped'
    }

    return (
        <div>
            <div className="shadow p-4 mt-4">
                <h1>Sales Person History</h1>
                <select onChange={handlePersonChange} required
                    name="sales_persons" id="sales_persons"
                    className="form-select" value={selected_person} >
                    <option value="">Choose a Sales Person</option>
                    {sales_persons?.map(sP => {
                        return (
                            <option key={sP.id} value={sP.id}>
                                {sP.name}
                            </option>
                        );
                    })}
                </select>
                <table className={tableClass}>
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Sales Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales_records?.map(fSR => {
                            if (fSR.sales_person.id == selected_person) {
                                return (
                                    <tr key={fSR.id}>
                                        <td>{fSR.sales_person.name}</td>
                                        <td>{fSR.sales_customer.name}</td>
                                        <td>{fSR.automobile.vin}</td>
                                        <td>${fSR.price}</td>
                                    </tr>
                                )
                            }
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SelectSalesRecords
