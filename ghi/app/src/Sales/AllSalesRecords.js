import { useEffect , useState} from "react";
import React from 'react';

function AllSalesRecords () {
    const [sales_records, setSalesRecord] = useState([]);

    useEffect(() => {
        const url = 'http://localhost:8090/api/salesrecords/'
        fetch(url)
        .then(response => {
                if(response.ok) {
                    let data = response.json()
                    return data
                }
                throw new Error ("BAD RESPONSE")

        })
        .then(data => {setSalesRecord(data.sales_records)})
        .catch((err) => console.log(err))
    }, [])

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Purchaser</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales_records?.map(sR => {
                        return(
                            <tr key={sR.id}>
                                <td>{sR.sales_person.name}</td>
                                <td>{sR.sales_person.employee_number}</td>
                                <td>{sR.sales_customer.name}</td>
                                <td>{sR.automobile.vin}</td>
                                <td>${sR.price}</td>
                            </tr>
                        )

                    })}
                </tbody>
            </table>
        </div>
    )

}

export default AllSalesRecords
