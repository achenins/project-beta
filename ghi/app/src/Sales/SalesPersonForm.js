import { useEffect , useState} from "react";
import React from 'react';

function SalesPersonForm() {
    const [name, setName] = useState('')
    const [employee_number, setEmployeeNumber] = useState('')
    const [submitted, setSubmitted] = useState(false)

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value
        setEmployeeNumber(value);
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const newSalesPerson = {
            "name": name,
            "employee_number": employee_number,
        }

        const salesPersonURL = 'http://localhost:8090/api/salespersons/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newSalesPerson),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        fetch(salesPersonURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setName('')
                setEmployeeNumber('')
                setSubmitted(true)
            })
            .catch(e => console.error('ERROR: ', e))
    }

    let messageClasses = "alert alert-success d-none mb-0"
    if(submitted === true){
        messageClasses = "alert alert-success mb-0"
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Sales Person</h1>
                    <form onSubmit={handleSubmit} id="create-salesPerson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required
                            type="text" name ="name" id="name"
                            className="form-control" value={name}/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumberChange} placeholder="Employee Number" required
                            type="number" name ="employee_number" id="employee_number"
                            className="form-control" value={employee_number}/>
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div><br/>
                <div className={messageClasses} id="success-message">
                    Success! New Sales Person Added!
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm
