import { useState } from "react";
import React from 'react';

function SalesCustomerForm() {
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')
    const [submitted, setSubmitted] = useState(false)

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    }

    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value);
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value);
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const newSalesCustomer = {
            "name": name,
            "address": address,
            "phone_number": phone_number
        }

        const salesCustomerURL = 'http://localhost:8090/api/salescustomers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newSalesCustomer),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        fetch(salesCustomerURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setName('')
                setAddress('')
                setPhoneNumber('')
                setSubmitted(true)
            })
            .catch(e => console.error('ERROR: ', e))
    }

    let messageClasses = "alert alert-success d-none mb-0"
    if(submitted === true){
        messageClasses = "alert alert-success mb-0"
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Sales Customer</h1>
                    <form onSubmit={handleSubmit} id="create-salesCustomer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required
                            type="text" name ="name" id="name"
                            className="form-control" value={name}/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} placeholder="Address" required
                            type="text" name ="address" id="address"
                            className="form-control" value={address}/>
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneNumberChange} placeholder="Phone Number" required
                            type="number" name ="phone_number" id="phone_number"
                            className="form-control" value={phone_number}/>
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div><br/>
                <div className={messageClasses} id="success-message">
                    Success! New Sales Customer Added!
                </div>
            </div>
        </div>
    );
}

export default SalesCustomerForm
