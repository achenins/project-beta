import React from 'react';

class ServiceAppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: '',
            owner_name: '',
            technician: '',
            technicians: [],
            dateTime: '',
            reason: '',
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data["automobile"] = data["vin"]
        data["datetime_of_appointment"] = data["dateTime"]
        data["datetime_of_appointment"] += "+00:00"
        data["reason_for_appointment"] = data["reason"]
        delete data.technicians;
        delete data.dateTime;
        delete data.reason;

        const serviceAppointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(serviceAppointmentUrl, fetchConfig);
        if (response.ok) {
            const newServiceAppointment = await response.json();

            const cleared = {
                vin: '',
                owner_name: '',
                technician: '',
                dateTime: '',
                reason: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            this.setState({ technicians: data.technicians });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Service Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-service-appointment-form" >
                            <div className="form-floating mb-3">
                                <input value={this.state.vin} onChange={this.handleInputChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN #</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.owner_name} onChange={this.handleInputChange} placeholder="owner_name" required type="text" name="owner_name" id="owner_name" className="form-control" />
                                <label htmlFor="owner_name">Owner Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.dateTime} onChange={this.handleInputChange} placeholder="dateTime" required type="datetime-local" name="dateTime" id="dateTime" className="form-control" />
                                <label htmlFor="dateTime">Date & Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <textarea value={this.state.reason} onChange={this.handleInputChange} placeholder="reason" name="reason" id="reason" className="form-control" rows="3" />
                                <label htmlFor="reason" className="form-label">Reason</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.technician} onChange={this.handleInputChange} required name="technician" id="technician" className="form-select">
                                    <option value="">Choose a Technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.employee_number} value={technician.employee_number}>
                                                {technician.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServiceAppointmentForm;
