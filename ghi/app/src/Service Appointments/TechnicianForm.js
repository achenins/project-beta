import { useEffect, useState } from "react";
import React from 'react';

function TechnicianForm() {
    const [submitted, setSubmitted] = useState(false)
    const [name, setName] = useState('')
    const [employee_number, setEmployeeNumber] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value
        setEmployeeNumber(value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const newTechnician = {
            "name": name,
            "employee_number": employee_number,
        }

        const technicianURL = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newTechnician),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        fetch(technicianURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setName('')
                setEmployeeNumber('')
                setSubmitted(true)
            })
            .catch(e => console.error('Error: ', e))
    }

    let messages = "alert alert-success d-none mb-0"

    if (submitted === true) {
        messages = "alert alert-success mb-0"
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required
                                type="text" name="name" id="name"
                                className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumberChange} placeholder="Employee Number" required
                                type="number" name="employee_number" id="employee_number"
                                className="form-control" value={employee_number} />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
                <div className={messages} id="success-message">
                    A new Technician has been created.
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm
