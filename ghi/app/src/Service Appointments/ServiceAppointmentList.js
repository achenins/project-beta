import { React, useEffect, useState } from "react";

function ServiceAppointmentList() {
    const [appointments, setAppointments] = useState([])

    const getList = async () => {
        const appointmentsUrl = "http://localhost:8080/api/appointments/"
        const response = await fetch(appointmentsUrl)
        if (response.ok) {
            const entireList = await response.json()
            setAppointments(entireList.appointments)
        }
    }

    useEffect(() => {
        getList()
    }, [])

    const handleComplete = async (vin, id) => {
        const completeURL = `http://localhost:8080/api/appointments/${vin}/${id}/`
        const completeConfig = {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const putResponse = await fetch(completeURL, completeConfig)
        if (putResponse.ok) {
            getList()

        }
    }

    const handleDelete = async (vin, id) => {
        const appointmentsUrl = `http://localhost:8080/api/appointments/${vin}/${id}/`
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const deleteResponse = await fetch(appointmentsUrl, fetchConfig)
        if (deleteResponse.ok) {
            getList()
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Owner Name</th>
                    <th>Date & Time</th>
                    <th>Assigned Technician</th>
                    <th>VIP</th>
                    <th>Reason for Service</th>
                </tr>
            </thead>
            <tbody>
                {appointments && appointments.filter(appointment => appointment.status == "upcoming").map(appointment => {

                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.owner_name}</td>
                            <td>{appointment.datetime_of_appointment}</td>
                            <td>{appointment.technician.name} </td>
                            <td>{appointment.vip_status ? "Yes" : "No"}</td>
                            <td>{appointment.reason_for_appointment}</td>
                            <td><button onClick={() => handleDelete(appointment.vin, appointment.id)} className="btn btn-danger">Cancel</button></td>
                            <td><button onClick={() => handleComplete(appointment.vin, appointment.id)} className="btn btn-success">Complete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ServiceAppointmentList
