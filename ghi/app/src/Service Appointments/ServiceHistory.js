import { React, useEffect, useState } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const [vin, setVin] = useState('')
    const [filteredList, setFilteredList] = useState([])

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    useEffect(() => {
        const url = "http://localhost:8080/api/appointments/"
        fetch(url)
            .then((response) => response.json())
            .then(data => { setAppointments(data.appointments) })
    }, [])

    const handleVinSearch = async (event) => {
        setFilteredList(appointments.filter(appointment => appointment.vin == vin))
    }

    return (
        <div className="input-group">
            <input onChange={handleVinChange} value={vin} placeholder="Please enter your Vin number" required type="text" name="vin" id="vin" className="form-control" />
            <button onClick={handleVinSearch}> Search </button>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Owner Name</th>
                        <th>Date & Time</th>
                        <th>Assigned Technician</th>
                        <th>VIP</th>
                        <th>Reason for Service</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments && filteredList.map(appointment => {
                        if (appointment.status === "completed") {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.owner_name}</td>
                                    <td>{appointment.datetime_of_appointment}</td>
                                    <td>{appointment.technician.name} </td>
                                    <td>{appointment.vip_status ? "Yes" : "No"}</td>
                                    <td>{appointment.reason_for_appointment}</td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </div>
    );

}

export default ServiceHistory
