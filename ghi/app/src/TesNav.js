import { Link } from 'react-router-dom';

function TestNav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-success">
            <div className="container-fluid">
                <Link className="navbar-brand active" to="/">CarCar</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarScroll">
                    <ul className="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/">Home</a>
                        </li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" to="/manufacturer" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Manufacturer
                            </Link>
                            <ul className="dropdown-menu">
                                <li><Link className="dropdown-item" to="/manufacturer">View Manufacturers</Link></li>
                                <li><hr className="dropdown-divider" /></li>
                                <li><Link className="dropdown-item" to="/manufacturer/new">Add New Manufacturer</Link></li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" to="/vehicle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Vehicle
                            </Link>
                            <ul className="dropdown-menu">
                                <li><Link className="dropdown-item" to="/vehicle">View Vehicle Models</Link></li>
                                <li><hr className="dropdown-divider" /></li>
                                <li><Link className="dropdown-item" to="/vehicle/new">Add New Vehicle Model</Link></li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" to="/automobiles" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Automobiles
                            </Link>
                            <ul className="dropdown-menu">
                                <li><Link className="dropdown-item" to="/automobiles">View Automobiles</Link></li>
                                <li><hr className="dropdown-divider" /></li>
                                <li><Link className="dropdown-item" to="/automobiles/new">Add New Automobile</Link></li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" to="/salesrecords" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Sales
                            </Link>
                            <ul className="dropdown-menu">
                                <li><Link className="dropdown-item" to="/salesrecords">View All Sales Records</Link></li>
                                <li><Link className="dropdown-item" to="/salesrecords/select">View Sales Person Sales</Link></li>
                                <li><hr className="dropdown-divider" /></li>
                                <li><Link className="dropdown-item" to="/salesrecords/new">Add New Sales Record</Link></li>
                                <li><Link className="dropdown-item" to="/salespersons/new">Add New Sales Person</Link></li>
                                <li><Link className="dropdown-item" to="/salescustomers/new">Add New Sales Customer</Link></li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" to="/services" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Service Appointments
                            </Link>
                            <ul className="dropdown-menu">
                                <li><Link className="dropdown-item" to="/service/new">Add New Service Appointment</Link></li>
                                <li><hr className="dropdown-divider" /></li>
                                <li><Link className="dropdown-item" to="/service/appointments">View Upcoming Appointments</Link></li>
                                <li><Link className="dropdown-item" to="/service/history">View Appointment History</Link></li>
                                <li><Link className="dropdown-item" to="/service/technician">Add New Technician</Link></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default TestNav;
