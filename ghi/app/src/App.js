import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import TestNav from './TesNav';
import AutomobileList from './Inventory/AutomobileList';
import AutomobileForm from './Inventory/AutomobileForm';
import ManufactureList from './Inventory/ManfacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import VehicleModelList from './Inventory/VehicleModelList';
import VehicleModelForm from './Inventory/VehicleModelForm';
import SalesRecordForm from './Sales/SalesRecordForm';
import SelectSalesRecords from './Sales/SelectSalesRecords';
import AllSalesRecords from './Sales/AllSalesRecords';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesCustomerForm from './Sales/SalesCustomerForm';
import ServiceAppointmentForm from './Service Appointments/ServiceAppointmentForm';
import ServiceAppointmentList from './Service Appointments/ServiceAppointmentList';
import ServiceHistory from './Service Appointments/ServiceHistory';
import TechnicianForm from './Service Appointments/TechnicianForm';

function App() {
  return (
    <BrowserRouter>
      <TestNav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/manufacturer" element={<ManufactureList />} />
          <Route path="/manufacturer/new" element={<ManufacturerForm />} />
          <Route path="/vehicle" element={<VehicleModelList />} />
          <Route path="/vehicle/new" element={<VehicleModelForm />} />
          <Route path="/salesrecords" element={<AllSalesRecords />} />
          <Route path="/salesrecords/new" element={<SalesRecordForm />} />
          <Route path="/salesrecords/select" element={<SelectSalesRecords />} />
          <Route path="/service/new" element={<ServiceAppointmentForm />} />
          <Route path="/service/appointments" element={<ServiceAppointmentList />} />
          <Route path="/service/history" element={<ServiceHistory />} />
          <Route path="/service/technician" element={<TechnicianForm />} />
          <Route path="/salespersons/new" element={<SalesPersonForm />} />
          <Route path="/salescustomers/new" element={<SalesCustomerForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
