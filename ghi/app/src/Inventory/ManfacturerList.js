import { useEffect , useState} from "react";
import React from 'react';

function ManufactureList() {

    const [manufacturers, setManufacturer] = useState([]);

    useEffect(() => {
        const url = 'http://localhost:8100/api/manufacturers/'
        fetch(url)
        .then(response => {
                if(response.ok) {
                    let data = response.json()
                    return data
                }
                throw new Error ("BAD RESPONSE")

        })
        .then(data => {setManufacturer(data.manufacturers)})
        .catch((err) => console.log(err))
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers && manufacturers.map(manufacturer => {
            return(
                <tr key={manufacturer.href}>
                    <td>{manufacturer.name}</td>
                </tr>
            )
          })}
        </tbody>
      </table>
    );
  }

  export default ManufactureList;
