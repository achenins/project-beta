import React from 'react';

class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            submitted: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.submitted;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();

            const cleared = {
                name: '',
                submitted: true
              };
              this.setState(cleared);
        }
    }

    render() {

        let messageClasses = "alert alert-success d-none mb-0"
        if(this.state.submitted === true){
            messageClasses = "alert alert-success mb-0"
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a New Manufacturer</h1>
                        <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Name" required
                                    type="text" name="name" id="name"
                                    className="form-control" value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Create</button>
                        </form>
                    </div><br/>
                    <div className={messageClasses} id="success-message">
                        Success! New Manufacturer Added!
                    </div>
                </div>
            </div>
        );
    }
}

export default ManufacturerForm;
