import { React, useEffect, useState } from "react";

function AutomobileList() {

    const [autos, setauto] = useState([])

    useEffect(() => {
        const url = "http://localhost:8100/api/automobiles/"
        fetch(url)
            .then(response => {
                if (response.ok) {
                    let data = response.json()
                    return data
                }
                throw new Error("Bad Response")
            })
            .then(data => { setauto(data.autos) })
            .catch((err) => console.log(err))
    }, [])


    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {autos && autos.map(auto => {
                    return (
                        <tr key={auto.href}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default AutomobileList;
