import React from 'react';

class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            models: [],
            color: '',
            year: '',
            vin: '',
            model_id: '',
            submitted: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.models;
        delete data.submitted;

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json()

            const cleared = {
                color: '',
                year: '',
                vin: '',
                model_id: '',
                submitted: true
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/models/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models });

        }
    }

    render() {

        let messageClasses = "alert alert-success d-none mb-0"
        if(this.state.submitted === true){
            messageClasses = "alert alert-success mb-0"
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a New Automobile</h1>
                        <form onSubmit={this.handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <select onChange={this.handleInputChange} required
                                    name="model_id" id="model_id"
                                    className="form-control" value={this.state.model_id}>
                                    <option value="">Choose a model</option>
                                    {this.state.models.map((model) => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.manufacturer.name} {model.name}
                                            </option>
                                        )
                                    })}</select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Color" required
                                    type="text" name="color" id="color"
                                    className="form-control" value={this.state.color} />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Year" required
                                    type="text" name="year" id="year"
                                    className="form-control" value={this.state.year} />
                                <label htmlFor="name">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="VIN" required
                                    type="text" name="vin" id="vin"
                                    className="form-control" value={this.state.vin} />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Create Automobile</button>
                        </form>
                    </div><br/>
                    <div className={messageClasses} id="success-message">
                        Success! New Automobile Added!
                    </div>
                </div>
            </div>
        );
    }
}

export default AutomobileForm;
