import React from 'react';

class VehicleModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            picture_url: '',
            manufacturer_id: '',
            manufacturers: [],
            submitted: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();

          this.setState({manufacturers: data.manufacturers});

        }
      }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({[name]: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers;
        delete data.submitted;

        const vehicleModelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(vehicleModelUrl, fetchConfig);
        if (response.ok) {
            const newVehicleModel = await response.json();
            const cleared = {
                name: '',
                picture_url: '',
                manufacturer_id: '',
                submitted: true
              };
              this.setState(cleared);
        }
    }



    render() {

        let messageClasses = "alert alert-success d-none mb-0"
        if(this.state.submitted === true){
            messageClasses = "alert alert-success mb-0"
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Vehicle Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-vehicleModel-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Name" required
                                type="text" name ="name" id="name"
                                className="form-control" value={this.state.name}/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Picture Url"
                                required type="url" name="picture_url"  id="picture_url"
                                className="form-control" value={this.state.picture_url}/>
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleInputChange} required
                                name="manufacturer_id" id="manufacturer_id"
                                className="form-select" value={this.state.manufacturer_id}>
                                    <option value="">Choose a Manufacturer</option>
                                    {this.state.manufacturers && this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Create</button>
                        </form>
                    </div><br/>
                    <div className={messageClasses} id="success-message">
                        Success! New Vehicle Model Added!
                    </div>
                </div>
            </div>
        );
    }
}

export default VehicleModelForm;
