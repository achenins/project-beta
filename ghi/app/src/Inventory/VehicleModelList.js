import { useEffect , useState} from "react";
import React from 'react';

function VehicleModelList() {

    const [models, setVehicleModel] = useState([]);

    useEffect(() => {
        const url = 'http://localhost:8100/api/models/'
        fetch(url)
        .then(response => {
                if(response.ok) {
                    let data = response.json()
                    return data
                }
                throw new Error ("BAD RESPONSE")

        })
        .then(data => {setVehicleModel(data.models)})
        .catch((err) => console.log(err))
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture Url</th>
          </tr>
        </thead>
        <tbody>
          {models && models.map(model => {
            return(
                <tr key={model.href}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td><img src={model.picture_url} width="200" className= "img-fluid img-thumbnail" /></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    );
  }

  export default VehicleModelList;
