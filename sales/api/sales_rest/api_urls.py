from django.urls import path
from .views import (
    api_sales_persons,
    api_sales_person,
    api_sales_customers,
    api_sales_customer,
    api_sales_records,
    api_sales_record
)

urlpatterns = [
    path("salespersons/", api_sales_persons, name="api_sales_persons"),
    path("salespersons/<int:pk>/", api_sales_person, name="api_sales_person"),
    path("salescustomers/", api_sales_customers, name="api_sales_customers"),
    path("salescustomers/<int:pk>/", api_sales_customer, name="api_sales_customer"),
    path("salesrecords/", api_sales_records, name="api_sales_records"),
    path("salesrecords/<int:pk>/", api_sales_record, name="api_sales_record"),
]
