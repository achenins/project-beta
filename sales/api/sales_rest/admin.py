from django.contrib import admin
from .models import *

admin.site.register(AutomobileVO)
admin.site.register(SalesPerson)
admin.site.register(SalesCustomer)
admin.site.register(SalesRecord)
