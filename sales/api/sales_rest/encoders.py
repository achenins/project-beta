from common.json import ModelEncoder

from .models import (
    AutomobileVO,
    SalesPerson,
    SalesCustomer,
    SalesRecord,
)

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "year",
        "color",
        "vin",
        "ispurchased",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class SalesCustomerEncoder(ModelEncoder):
    model = SalesCustomer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "automobile",
        "sales_person",
        "sales_customer",
        "price",
        "created",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "sales_customer": SalesCustomerEncoder(),
    }
