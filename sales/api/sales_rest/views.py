from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    SalesPersonEncoder,
    SalesCustomerEncoder,
    SalesRecordEncoder,
)
from .models import (
    AutomobileVO,
    SalesPerson,
    SalesCustomer,
    SalesRecord,
)


@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create Sales Person"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)

            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales_customers(request):
    if request.method == "GET":
        sales_customers = SalesCustomer.objects.all()
        return JsonResponse(
            {"sales_customers": sales_customers},
            encoder=SalesCustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_customer = SalesCustomer.objects.create(**content)
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create Sales Customer"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_customer(request, pk):
    if request.method == "GET":
        try:
            sales_customer = SalesCustomer.objects.get(id=pk)
            return JsonResponse(
                sales_customer, encoder=SalesCustomerEncoder, safe=False
            )
        except SalesCustomer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_customer = SalesCustomer.objects.get(id=pk)
            sales_customer.delete()
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except SalesCustomer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            sales_customer = SalesCustomer.objects.get(id=pk)

            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_customer, prop, content[prop])
            sales_customer.save()
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except SalesCustomer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            auto_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=auto_vin)
            content["automobile"] = automobile

            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(pk=sales_person_id)
            content["sales_person"] = sales_person

            sales_customer_id = content["sales_customer"]
            sales_customer = SalesCustomer.objects.get(pk=sales_customer_id)
            content["sales_customer"] = sales_customer

            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create Sales Record"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_sales_record(request, pk):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(sales_record, encoder=SalesRecordEncoder, safe=False)
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            sales_record.delete()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
